$(document).ready(function() {
  $.ajax({
      method: "POST",
      url: "../api/get.php",
    })
    .done(data => {
      let result = $.parseJSON(data);
      let string = '<div class="table"><table id="match_data"><thead><tr><th>Player 1</th><th>Score</th><th>Player 2</th><th>Score</th></tr></thead>';
      /* from result create a string of data and append to the div */
      $.each(result, (key, value) =>{
        string += '<tr><td class="name">' + value['player_1'] + '</td><td> ' + value['player_1_score'] + '</td><td class="name">' + value['player_2'] + '</td><td>' + value['player_2_score'] + '</td></tr>';
      });
      string += '</table></div>';
      $("#records").html(string);
      window.setTimeout(function(){$('#match_data').DataTable()}, 50);
    });

  $("#submit").click(function() {
    let player_1 = $("#player_1").val();
    let player_1_score = $("#player_1_score").val();
    let player_2 = $("#player_2").val();
    let player_2_score = $("#player_2_score").val();
    // Returns successful data submission message when the entered information is stored in database.
    let dataString = 'player_1=' + player_1 + '&player_1_score=' + player_1_score + '&player_2=' + player_2 + '&player_2_score=' + player_2_score;
    if (player_1 == '' || player_1_score == '' || player_2 == '' || player_2_score == '') {
      alert("Please Fill All Fields");
    } else {
      // AJAX Code To Submit Form.
      $.ajax({
        method: "POST",
        url: "../api/post.php",
        data: dataString,
        cache: false,
        success: result => {
          alert(result);
        }
      });
      location.reload();
    }
    return false;
  });
});
