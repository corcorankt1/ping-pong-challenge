<?php

$host         = "localhost";
$username     = "root";
$password     = "root";
$dbname       = "ping_pong_games";
$result_array = array();
/* Create connection */
$conn = new mysqli($host, $username, $password, $dbname);
/* Check connection  */
if ($conn->connect_error) {
     die("Connection to database failed: " . $conn->connect_error);
}
/* Get game results from database */
$sql = "SELECT * FROM games ";
$result = $conn->query($sql);
/* If there are results from database push to result array */
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        array_push($result_array, $row);
    }
}
/* send JSON encoded array to client */
echo json_encode($result_array);

$conn->close();

?>
